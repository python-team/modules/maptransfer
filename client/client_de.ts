<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de_DE">
<context>
    <name>Gui</name>
    <message>
        <location filename="client.py" line="62"/>
        <source>No downloads are running.</source>
        <translation>Keine Downloads gestartet.</translation>
    </message>
    <message>
        <location filename="client.py" line="65"/>
        <source>No uploads are running.</source>
        <translation>Keine Uploads gestartet.</translation>
    </message>
    <message>
        <location filename="client.py" line="200"/>
        <source>Maps are being saved to:
{0}</source>
        <translation type="obsolete">Maps werden gespeichert in:
{0}</translation>
    </message>
    <message>
        <location filename="client.py" line="221"/>
        <source>Downloading {0}</source>
        <translation type="obsolete">Lade {0} herunter</translation>
    </message>
    <message>
        <location filename="client.py" line="234"/>
        <source>Map {0} cannot be downloaded. The server said:
{1}: {2}

{3}</source>
        <translation type="obsolete">Map {0} kann nicht heruntergeladen werden. Der Server sagte:
{1}: {2}

{3}</translation>
    </message>
    <message>
        <location filename="client.py" line="242"/>
        <source>All downloads finished</source>
        <translation>Alle Downloads fertig</translation>
    </message>
    <message>
        <location filename="client.py" line="247"/>
        <source>Uploading {0}</source>
        <translation type="obsolete">Lade {0} hoch</translation>
    </message>
    <message>
        <location filename="client.py" line="260"/>
        <source>Map {0} cannot be uploaded. The server said:
{1}: {2}

{3}</source>
        <translation type="obsolete">Map {0} kann nicht hochgeladen werden. Der Server sagte:
{1}: {2}

{3}</translation>
    </message>
    <message>
        <location filename="client.py" line="268"/>
        <source>All uploads finished</source>
        <translation>Alle Uploads fertig</translation>
    </message>
    <message>
        <location filename="client.py" line="296"/>
        <source>Map {0} exists - overwrite it?</source>
        <translation type="obsolete">Map {0} existiert - überschreiben?</translation>
    </message>
    <message>
        <location filename="client.py" line="330"/>
        <source>Map {0} already exists on the server and will not be uploaded.</source>
        <translation type="obsolete">Map {0} existiert bereits auf dem Server und wird daher nicht hochgeladen.</translation>
    </message>
    <message>
        <location filename="client.py" line="200"/>
        <source>Maps are being saved to:
%1</source>
        <translation>Maps werden gespeichert in:
%1</translation>
    </message>
    <message>
        <location filename="client.py" line="221"/>
        <source>Downloading %1</source>
        <translation>Lade %1 herunter</translation>
    </message>
    <message>
        <location filename="client.py" line="234"/>
        <source>Map %1 cannot be downloaded. The server said:
%2: %3

%4</source>
        <translation>Map %1 kann nicht heruntergeladen werden. Der Server sagte:
%2: %3

%4</translation>
    </message>
    <message>
        <location filename="client.py" line="247"/>
        <source>Uploading %1</source>
        <translation>Lade %1 hoch</translation>
    </message>
    <message>
        <location filename="client.py" line="260"/>
        <source>Map %1 cannot be uploaded. The server said:
%2: %3

%4</source>
        <translation>Map %1 kann nicht hochgeladen werden. Der Server sagte:
%2: %3

%4</translation>
    </message>
    <message>
        <location filename="client.py" line="296"/>
        <source>Map %1 exists - overwrite it?</source>
        <translation>Map %1 existiert - überschreiben?</translation>
    </message>
    <message>
        <location filename="client.py" line="330"/>
        <source>Map %1 already exists on the server and will not be uploaded.</source>
        <translation>Map %1 existiert bereits auf dem Server und wird daher nicht hochgeladen.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="client.ui" line="132"/>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="client.ui" line="24"/>
        <source>Found path to Steam: D:\Yadda\Yadda\Steam</source>
        <translation>Pfad zu Steam: D:\Yadda\Yadda\Steam</translation>
    </message>
    <message>
        <location filename="client.ui" line="31"/>
        <source>Steam Account</source>
        <translation>Steam-Account</translation>
    </message>
    <message>
        <location filename="client.ui" line="45"/>
        <source>Game Server</source>
        <translation>Game-Server</translation>
    </message>
    <message>
        <location filename="client.ui" line="75"/>
        <source>Maps on the Client:</source>
        <translation>Maps auf dem Client:</translation>
    </message>
    <message>
        <location filename="client.ui" line="82"/>
        <source>Maps on the Server:</source>
        <translation>Maps auf dem Server:</translation>
    </message>
    <message>
        <location filename="client.ui" line="155"/>
        <source>Select all</source>
        <translation>Alle auswählen</translation>
    </message>
    <message>
        <location filename="client.ui" line="14"/>
        <source>MapTransfer</source>
        <translation>MapTransfer</translation>
    </message>
    <message>
        <location filename="client.ui" line="21"/>
        <source>The base path where Steam is installed.</source>
        <translation>Das Verzeichnis in dem Steam installiert ist.</translation>
    </message>
    <message>
        <location filename="client.ui" line="38"/>
        <source>The Steam account to choose games and maps from.</source>
        <translation>Der Steam-Account aus dem Spiele und Maps ausgewählt werden sollen.</translation>
    </message>
    <message>
        <location filename="client.ui" line="52"/>
        <source>The game server to upload the maps to.</source>
        <translation>Der Game-Server auf den die Maps hochgeladen werden sollen.</translation>
    </message>
    <message>
        <location filename="client.ui" line="61"/>
        <source>Local maps in your game folder.</source>
        <translation>Maps in deinem lokalen Spielverzeichnis.</translation>
    </message>
    <message>
        <location filename="client.ui" line="68"/>
        <source>Remote maps on the server.</source>
        <translation>Maps auf dem Server.</translation>
    </message>
    <message>
        <location filename="client.ui" line="106"/>
        <source>Select all maps that are not available on the server.</source>
        <translation>Markiert alle Maps, die nicht auf dem Server vorhanden sind.</translation>
    </message>
    <message>
        <location filename="client.ui" line="129"/>
        <source>Start transferring the selected maps.</source>
        <translation>Beginne die Übertragung der ausgewählten Maps.</translation>
    </message>
    <message>
        <location filename="client.ui" line="152"/>
        <source>Select all maps that are not available on the client.</source>
        <translation>Markiert alle Maps, die nicht lokal vorhanden sind.</translation>
    </message>
</context>
</TS>

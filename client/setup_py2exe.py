﻿# -*- coding: utf-8 -*-

""" Distutils script for use with py2exe in order to build a distributable
    ZIP package containing everything needed to run the MapTransfer client.

    Copyright © 2009-2010, Michael "Svedrin" Ziegler <diese-addy@funzt-halt.net>
"""

from distutils.core import setup
import py2exe

setup(
	version = "0.3",
	description = "Uploads maps from local Steam accounts to a gameserver. (Especially useful for lazy admins.)",
	name = "maptransfer",
	
	console = [
		{ "script": "client.py" }
		],
	
	data_files = [
		'maptransfer.example.cfg',
		'client_de.qm'
		],
	
	options = {
		"py2exe": {
			"includes":["sip"]
			}
		}
	)


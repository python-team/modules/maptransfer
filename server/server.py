#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" MapTransfer (Server) -- upload/download maps to/from a VALVe game server

    Copyright © 2009-2010, Michael "Svedrin" Ziegler <diese-addy@funzt-halt.net>
"""

import os
import re
import base64
import bz2
import shutil
import socket
import threading
import time

from SimpleHTTPServer import SimpleHTTPRequestHandler
from BaseHTTPServer   import HTTPServer
from ConfigParser     import ConfigParser
from SocketServer     import ThreadingMixIn
from os.path          import exists
from OpenSSL          import SSL

from shlox            import shlox


def link_file( src, dest ):
	""" Link a file, after checking the file exists and the destination does not. """
	if exists( src ) and not exists( dest ):
		os.link( src, dest )

class Conf( object, ConfigParser ):
	""" Context file processor that eases access to certain fields. """
	
	def __init__( self, filename ):
		ConfigParser.__init__( self )
		self.filename = filename
		self.read( filename )
	
	def get_maps_dir( self, server, num = 0 ):
		""" Get a map directory entry for the given server. """
		mdirs = list( shlox( self.gs( server, "mapdir" ) ) )
		return mdirs[num]
	
	def get_maps_dir_list( self, server, num = 0 ):
		""" Get a list of map directories for the given server. """
		mdirs = list( shlox( self.gs( server, "mapdir" ) ) )
		if num == 0:
			return mdirs
		else:
			return mdirs[num:]
	
	def has_server( self, server ):
		""" Check if a section for this game server can be found. """
		return self.has_section( ':'+server )
	
	def gs( self, server, field ):
		""" Get a field's value for the given game server. """
		return self.get( ':'+server, field )
	
	known_servers = property(
		lambda self: [ ident[1:] for ident in self.sections() if ident[0] == ':' ],
		doc="A list of all known server identifiers."
		)


class SecureHTTPRequestHandler( SimpleHTTPRequestHandler ):
	""" Extends the SimpleHTTPRequestHandler to be used with a SecureHTTPServer. """
	
	def setup(self):
		self.connection = self.request
		self.rfile = socket._fileobject(self.request, "rb", self.rbufsize)
		self.wfile = socket._fileobject(self.request, "wb", self.wbufsize)


class MapHTTPHandler( SecureHTTPRequestHandler ):
	""" Handles incoming HTTP requests. """
	
	pathregex = re.compile( '/[\w\d]+/[\w\d_-]+\.[\w\d]+' )
	
	def check_auth( self ):
		""" Check if the client is authenticated and authorized for access. """
		
		if "authorization" not in self.headers:
			self.send_response( 401, "Unauthorized." )
			self.send_header( "WWW-Authenticate", 'Basic realm="MapTransfer"' )
			self.end_headers()
			self.wfile.write( "Please authenticate yourself." )
			return False
		
		else:
			authname, authpass = base64.b64decode( self.headers["authorization"] ).split(':')
			
			# check for unknown user or wrong password
			if not MapHTTPHandler.conf.has_option( "users", authname ) or \
			   MapHTTPHandler.conf.get( "users", authname ) != authpass:
				self.send_response( 401, "Unauthorized." )
				self.send_header( "WWW-Authenticate", 'Basic realm="MapTransfer"' )
				self.end_headers()
				self.wfile.write( "Please authenticate yourself." )
				return False
			
			else:
				return True
	
	
	def do_GET( self ):
		""" Handle a GET request: a client wants to download a file from us. """
		
		if self.path == "/":
			# send server list
			self.send_response( 200, "OK" )
			self.end_headers()
			
			for serv in MapHTTPHandler.conf.known_servers:
				self.wfile.write( "%s:%s:%s\r\n" % ( serv,       # server identifier
					MapHTTPHandler.conf.gs( serv, "game" ), # gametag
					MapHTTPHandler.conf.gs( serv, "name" )  # human-readable name
					) )
		
		# path: /ident - check if ident is a known identifier, if so send map list
		elif self.path[0] == '/' and MapHTTPHandler.conf.has_server( self.path[1:] ):
			basedir = MapHTTPHandler.conf.get_maps_dir( self.path[1:] )
			
			files = os.listdir( basedir )
			
			fileslist = dict()
			for thisfile in files:
				if not os.path.isfile( "%s/%s" % ( basedir, thisfile ) ):
					continue
				base, ext = thisfile.split( '.', 1 )
				if base not in fileslist:
					fileslist[base] = [ext]
				else:
					fileslist[base].append( ext )
			
			self.send_response( 200, "OK" )
			self.end_headers()
			for mapname in fileslist:
				if "bsp" in fileslist[mapname]:
					self.wfile.write( "%s:%s\n" % ( mapname, ' '.join( fileslist[mapname] ) ) )
		
		# This is probably a path /srv/mapname.something, and probably one that we know
		elif MapHTTPHandler.pathregex.match( self.path ):
			
			_, srvpath, filename = self.path.split( "/", 2 )
			
			if MapHTTPHandler.conf.has_server( srvpath ) and \
			   os.path.isfile( "%s/%s" % ( MapHTTPHandler.conf.get_maps_dir( srvpath ), filename ) ):
				# path is valid, server is known and file exists: send the file
				self.send_response( 200, "OK" )
				self.send_header( "Content-Type", "application/octet-stream" )
				self.end_headers()
				
				diskfd = open( "%s/%s" % ( MapHTTPHandler.conf.get_maps_dir( srvpath ), filename ), 'rb' )
				try:
					shutil.copyfileobj( diskfd, self.wfile )
				finally:
					diskfd.close()
			
			else:
				self.send_response( 404, "File not found." )
				self.end_headers()
				self.wfile.write( "File not found. Searched for Server '%s' and Map '%s'." % ( srvpath, filename ) )
		
		# Else: dunno.
		else:
			self.send_response( 404, "File not found." )
			self.end_headers()
			self.wfile.write( "File not found." )
	
	
	
	def do_PUT( self ):
		""" Handle a PUT request: a client wants to upload a file to us. """
		
		if not MapHTTPHandler.pathregex.match( self.path ):
			self.send_response( 400, "Bad Request." )
			self.end_headers()
			self.wfile.write( "You tried to PUT the main path of a server or a server itself, but you "
				"can only upload map files as everything else is configured statically." )
			return
		
		_, srvpath, filename = self.path.split( "/", 2 )
		
		if not MapHTTPHandler.conf.has_server( srvpath ):
			self.send_response( 404, "No such server: '%s'." % srvpath )
			self.end_headers()
			self.wfile.write( "The specified server '%s' does not exist." % srvpath )
			return
		
		mdir = MapHTTPHandler.conf.get_maps_dir( srvpath )
		
		fullpath = "%s/%s" % ( mdir, filename )
		
		if os.path.commonprefix( [ fullpath, mdir ] ) != mdir:
			# due to the regex above, this sort of can't possibly fail - but you never know.
			self.send_response( 403, "Forbidden." )
			self.end_headers()
			self.wfile.write( "What are you doing?" )
			return
		
		if os.path.isfile( fullpath ):
			self.send_response( 409, "Map exists: '%s'." % filename )
			self.end_headers()
			self.wfile.write("The specified map '%s' already exists on the server and therefore won't be accepted." % filename)
			return
		
		if not self.check_auth():
			return
		
		# check the ending - we only accept nav or txt or res files if a bsp exists, and never accept anything else
		base, ext = filename.split( '.', 1 )
		
		if ext not in ( 'bsp', 'bsp.bz2' ):
			if not ( ext in ( 'nav', 'res', 'txt' ) and \
			   os.path.isfile( "%s/%s.bsp" % ( MapHTTPHandler.conf.get_maps_dir( srvpath ), base ) ) ):
				self.send_response( 403, "Forbidden." % filename )
				self.end_headers()
				self.wfile.write( "Either this is not a map file, or you are trying to upload resources before the actual map." )
				return
		
		print "File upload accepted."
		
		# Request seems fine, let's upload the map!
		
		if not "x-dry-run" in self.headers:
			basepath = "%s/%s" % ( mdir, base )
			
			rfile_content = self.rfile.read( int( self.headers['content-length'] ) )
			
			print "File upload completed (read %d bytes), processing..." % len( rfile_content )
			
			if not rfile_content:
				self.send_response( 400, "Bad Request." )
				self.end_headers()
				self.wfile.write( "Where's the data?" )
				return
			
			diskfd = open( fullpath, 'wb' )
			try:
				diskfd.write( rfile_content )
			finally:
				diskfd.close()
			
			# We were served one file, but we want to have both a bsp and a bsp.bz2.
			if ext == "bsp.bz2":
				bsp_fd = open( "%s.bsp" % basepath, 'wb' )
				bsp_fd.write( bz2.decompress( rfile_content ) )
				bsp_fd.close()
			
			elif ext == "bsp":
				bz2_fd = open( "%s.bsp.bz2" % basepath, 'wb' )
				bz2_fd.write( bz2.compress( rfile_content ) )
				bz2_fd.close()
			
			
			# If there's more than one dir in the tuple, hardlink the Map to all other dirs
			for thedir in MapHTTPHandler.conf.get_maps_dir_list( srvpath, 1 ):
				link_file( fullpath, "%s/%s" % ( thedir, filename ) )
				
				if ext == "bsp.bz2":
					link_file( "%s.bsp" % basepath, "%s/%s.bsp" % ( thedir, base ) )
				elif ext == "bsp":
					link_file( "%s.bsp.bz2" % basepath, "%s/%s.bsp.bz2" % ( thedir, base ) )
		
		self.send_response( 201, "Created." )
		self.send_header( "Location", "/%s/%s.bsp.bz2" % ( srvpath, base ) )
		self.end_headers()
		self.wfile.write( "Map uploaded successfully." )


class SecureHTTPServer( HTTPServer ):
	""" HTTPServer extension that provides SSL security.
	
	    See <http://code.activestate.com/recipes/442473/>.
	"""
	def __init__( self, server_address, handler_class ):
		HTTPServer.__init__( self, server_address, handler_class )
		
		ctx = SSL.Context( SSL.SSLv23_METHOD )
		
		ctx.use_privatekey_file(  handler_class.conf.get( "general", "sslkey"  ) )
		ctx.use_certificate_file( handler_class.conf.get( "general", "sslcert" ) )
		
		self.socket = SSL.Connection( ctx,
			socket.socket(self.address_family, self.socket_type)
			)
		
		self.server_bind()
		self.server_activate()



class ThreadedHTTPServer( ThreadingMixIn, SecureHTTPServer ):
	""" Process each query in a separate thread, to allow serving multiple requests at once. """
	pass

class IPv6ThreadedHTTPServer( ThreadedHTTPServer ):
	""" Bind on an IPv6 socket. """
	address_family = socket.AF_INET6


if __name__ == '__main__':
	from sys import argv
	
	if len( argv ) > 1:
		cfg = argv[1]
	else:
		cfg = "server.cfg"
	
	if not exists( cfg ):
		print "The configuration file '%s' does not exist!" % cfg
		import sys
		sys.exit(1)
	
	MapHTTPHandler.conf = Conf( cfg )
	
	address = MapHTTPHandler.conf.get(    "general", "address" )
	port    = MapHTTPHandler.conf.getint( "general", "port"    )
	
	if not address:
		address = ':: 0.0.0.0'
	
	servers = {
		socket.AF_INET:  ThreadedHTTPServer,
		socket.AF_INET6: IPv6ThreadedHTTPServer,
		}
	
	threads = []
	
	for addrstr in address.split(' '):
		(family, socktype, proto, canonname, sockaddr) = socket.getaddrinfo( addrstr, port, 0, socket.SOCK_STREAM )[0]
		httpd = servers[family]( sockaddr, MapHTTPHandler )
		thr   = threading.Thread( target=httpd.serve_forever, name=("Servant for %s" % sockaddr[0]) )
		thr.servant = httpd
		threads.append(thr)
		thr.start()
	
	try:
		while(True):
			time.sleep(999999)
	except KeyboardInterrupt:
		print "Shutting down."
		for thr in threads:
			if hasattr( thr.servant, "shutdown" ):
				thr.servant.shutdown()
			else:
				print "Can't shutdown the servant. You might want to use Python >=2.6. Hit ^\ to kill me."
			thr.join()


